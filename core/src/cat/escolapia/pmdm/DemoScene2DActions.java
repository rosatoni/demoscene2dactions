package cat.escolapia.pmdm;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.ParallelAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateToAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;


public class DemoScene2DActions  implements ApplicationListener, InputProcessor {

    public class ActorDeTest extends Actor {
        Texture texture = new Texture(Gdx.files.internal("badlogic.jpg"));

        public ActorDeTest(){
            setBounds(getX(),getY(),texture.getWidth(),texture.getHeight());
        }

        @Override
        public void draw(Batch batch, float alpha){
            batch.draw(texture,this.getX(),getY(),this.getOriginX(),this.getOriginY(),this.getWidth(),
                    this.getHeight(),this.getScaleX(), this.getScaleY(),this.getRotation(),0,0,
                    texture.getWidth(),texture.getHeight(),false,false);
        }
    }


    private Stage stage;
    private ActorDeTest actorDeTest;
    MoveToAction moveToLeft, moveToRight;
    RotateToAction rotate;

    @Override
    public void create() {
        stage = new Stage();
        actorDeTest = new ActorDeTest();

        actorDeTest.setPosition(Gdx.graphics.getWidth()/2 - actorDeTest.getWidth()/2,
                Gdx.graphics.getHeight()/2 - actorDeTest.getHeight()/2);

        moveToLeft = new MoveToAction();
        moveToLeft.setPosition(10f, Gdx.graphics.getHeight() / 2 - actorDeTest.getHeight() / 2);
        moveToLeft.setDuration(2f);

        moveToRight = new MoveToAction();
        moveToRight.setPosition(Gdx.graphics.getWidth() - 10 - actorDeTest.getWidth(), Gdx.graphics.getHeight() / 2 - actorDeTest.getHeight() / 2);
        moveToRight.setDuration(2f);

        rotate = new RotateToAction();
        rotate.setRotation(360f);
        rotate.setDuration(1f);

        stage.addActor(actorDeTest);

        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void dispose() {
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }


    @Override
    public boolean keyDown(int keycode) {
        if(keycode == Input.Keys.LEFT) {
            if (actorDeTest.getActions().contains(moveToLeft, true)) {
                // Acció actual
            } else {
                moveToRight.reset();
                moveToLeft.reset();
                rotate.setRotation(360f);
                rotate.restart();
                actorDeTest.addAction(new ParallelAction(moveToLeft, rotate));
            }
        }

        if(keycode == Input.Keys.RIGHT) {
            if (actorDeTest.getActions().contains(moveToRight, true)) {
                // Acció actual
            } else {
                moveToLeft.reset();
                moveToRight.reset();
                rotate.setRotation(-360f);
                rotate.restart();
                actorDeTest.addAction(new ParallelAction(moveToRight, rotate));
            }
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}